<?php
/**
 * Created by PhpStorm.
 * User: dpaz
 * Date: 3/28/18
 * Time: 7:41 PM
 */

namespace dpazuic\curator_io\feeds;


class post
{
    private $id;
    private $network_id;
    private $status;
    private $flagged;
    private $has_media;
    private $source_type;
    private $source_identifier;
    private $source_created_at;
    private $user_screen_name;
    private $user_full_name;
    private $user_image;
    private $text;
    private $is_html;
    private $image;
    private $video;
    private $url;
    private $user_url;
    private $thumbnail;
    private $video_width;
    private $video_height;
    private $comments;
    private $views;
    private $is_repost;
    private $is_reply;
    private $is_deleted;
    private $likes;
    private $originator_user_screenname;
    private $originator_user_url;
    private $originator_post_url;
    private $pinned;
    private $longitude;
    private $latitude;
    private $location_name;
    private $image_width;
    private $image_height;
    private $image_processed;
    private $has_image;
    private $has_video;
    private $image_large;
    private $image_large_width;
    private $image_large_height;
    private $network_name;
    private $source_id;
    private $feed_id;
    private $last_modified;

    public function __construct($object)
    {

        if(empty($object)){

            throw new \Exception("Post object not found");

        }

        $this->mapProperties($object);

    }

    public function mapProperties($object)
    {

        foreach($object as $prop=>$val) {

            $this->{$prop} = $val;

        }

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNetworkId()
    {
        return $this->network_id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getFlagged()
    {
        return $this->flagged;
    }

    /**
     * @return mixed
     */
    public function getHasMedia()
    {
        return $this->has_media;
    }

    /**
     * @return mixed
     */
    public function getSourceType()
    {
        return $this->source_type;
    }

    /**
     * @return mixed
     */
    public function getSourceIdentifier()
    {
        return $this->source_identifier;
    }

    /**
     * @return mixed
     */
    public function getSourceCreatedAt()
    {
        return $this->source_created_at;
    }

    /**
     * @return mixed
     */
    public function getUserScreenName()
    {
        return $this->user_screen_name;
    }

    /**
     * @return mixed
     */
    public function getUserFullName()
    {
        return $this->user_full_name;
    }

    /**
     * @return mixed
     */
    public function getUserImage()
    {
        return $this->user_image;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getisHtml()
    {
        return $this->is_html;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getUserUrl()
    {
        return $this->user_url;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @return mixed
     */
    public function getVideoWidth()
    {
        return $this->video_width;
    }

    /**
     * @return mixed
     */
    public function getVideoHeight()
    {
        return $this->video_height;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @return mixed
     */
    public function getisRepost()
    {
        return $this->is_repost;
    }

    /**
     * @return mixed
     */
    public function getisReply()
    {
        return $this->is_reply;
    }

    /**
     * @return mixed
     */
    public function getisDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @return mixed
     */
    public function getOriginatorUserScreenname()
    {
        return $this->originator_user_screenname;
    }

    /**
     * @return mixed
     */
    public function getOriginatorUserUrl()
    {
        return $this->originator_user_url;
    }

    /**
     * @return mixed
     */
    public function getOriginatorPostUrl()
    {
        return $this->originator_post_url;
    }

    /**
     * @return mixed
     */
    public function getPinned()
    {
        return $this->pinned;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return mixed
     */
    public function getLocationName()
    {
        return $this->location_name;
    }

    /**
     * @return mixed
     */
    public function getImageWidth()
    {
        return $this->image_width;
    }

    /**
     * @return mixed
     */
    public function getImageHeight()
    {
        return $this->image_height;
    }

    /**
     * @return mixed
     */
    public function getImageProcessed()
    {
        return $this->image_processed;
    }

    /**
     * @return mixed
     */
    public function getHasImage()
    {
        return $this->has_image;
    }

    /**
     * @return mixed
     */
    public function getHasVideo()
    {
        return $this->has_video;
    }

    /**
     * @return mixed
     */
    public function getImageLarge()
    {
        return $this->image_large;
    }

    /**
     * @return mixed
     */
    public function getImageLargeWidth()
    {
        return $this->image_large_width;
    }

    /**
     * @return mixed
     */
    public function getImageLargeHeight()
    {
        return $this->image_large_height;
    }

    /**
     * @return mixed
     */
    public function getNetworkName()
    {
        return $this->network_name;
    }

    /**
     * @return mixed
     */
    public function getSourceId()
    {
        return $this->source_id;
    }

    /**
     * @return mixed
     */
    public function getFeedId()
    {
        return $this->feed_id;
    }

    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->last_modified;
    }

}
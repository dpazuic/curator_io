<?php
/**
 * Created by PhpStorm.
 * User: dpaz
 * Date: 3/28/18
 * Time: 5:34 PM
 */

namespace dpazuic\curator_io\sources;


class sourcesList
{
    private $list;

    public function __construct($data = null)
    {

        if(!is_array($data)){

            throw new \Exception("Sources JSON object array is null");

        }

        // Iterate over the $data array, setting each internal stdClass obj to a Feed Obj
        foreach($data as $source){

            $this->list[] = new \dpazuic\curator_io\sources\source($source);

        }

    }

    /**
     * @return mixed
     */
    public function getList()
    {
        return $this->list;
    }

}
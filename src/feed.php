<?php
/**
 * Created by PhpStorm.
 * User: dpaz
 * Date: 3/28/18
 * Time: 5:37 PM
 */

namespace dpazuic\curator_io\feeds;


class feed
{

    // todo - make widget_options and colour_options objects
//[widget_options] => {"type":"Waterfall","maxHeight":0,"postsPerPage":12,"lang":"en","postClickAction":"open-popup","showShareIcons":true,"showReadMorePost":false,"showReadMorePopup":false,"waterfall":{"showLoadMore":true,"continuousScroll":false,"gridWidth":300,"animate":true,"animateSpeed":400},"grid":{"minWidth":200,"showLoadMore":false,"rows":3},"carousel":{"autoPlay":true,"infinite":true,"minWidth":200},"panel":{},"filter":{"showNetworks":false,"networksLabel":"SourceNetworks:","showSources":false,"sourcesLabel":"Sources:"}}
//[colour_options] => {"headerBg":"#ffffff","headerText":"#333333","bodyBg":"#ffffff","bodyText":"#333333","bodyLinks":"#000000","icon":"#000000","userName":"","date":"#000000","footerText":"#000000","footerBg":"#ffffff","footerIcons":"#333333","loadMoreText":"#2b2b2b","loadMoreBg":"#ffffff","loadMoreBorder":"rgba(0,0,0,0.1)"}

    private $id;
    private $name;
    private $slug;
    private $public_key;
    private $moderation;
    private $post_status;
    private $colour_body_bg;
    private $colour_body_text;
    private $colour_header_bg;
    private $colour_header_text;
    private $widget_options;
    private $colour_options;
    private $default_image;
    private $default_user_image;
    private $widget_endpoint;
    private $debug;
    private $widget_version;
    private $html_before;
    private $html_after;

    public function __construct($object = null)
    {

        if(empty($object)){

            throw new \Exception("Object is null ");

        }

        if(!is_object($object)){

            throw new \Exception("Object is not valid");

        }

        $this->mapProperties($object);
    }

    /**
     * @param $object
     */
    public function mapProperties($object)
    {

        foreach($object as $prop=>$val) {

            if($prop == "widget_options" OR $prop == "colour_options") {

                $this->{$prop} = json_decode($val); // todo - Not handled very eloquently

            } else {

                $this->{$prop} = $val;

            }

        }

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getPublicKey()
    {
        return $this->public_key;
    }

    /**
     * @return mixed
     */
    public function getModeration()
    {
        return $this->moderation;
    }

    /**
     * @return mixed
     */
    public function getPostStatus()
    {
        return $this->post_status;
    }

    /**
     * @return mixed
     */
    public function getColourBodyBg()
    {
        return $this->colour_body_bg;
    }

    /**
     * @return mixed
     */
    public function getColourBodyText()
    {
        return $this->colour_body_text;
    }

    /**
     * @return mixed
     */
    public function getColourHeaderBg()
    {
        return $this->colour_header_bg;
    }

    /**
     * @return mixed
     */
    public function getColourHeaderText()
    {
        return $this->colour_header_text;
    }

    /**
     * @return mixed
     */
    public function getWidgetOptions()
    {
        return $this->widget_options;
    }

    /**
     * @return mixed
     */
    public function getColourOptions()
    {
        return $this->colour_options;
    }

    /**
     * @return mixed
     */
    public function getDefaultImage()
    {
        return $this->default_image;
    }

    /**
     * @return mixed
     */
    public function getDefaultUserImage()
    {
        return $this->default_user_image;
    }

    /**
     * @return mixed
     */
    public function getWidgetEndpoint()
    {
        return $this->widget_endpoint;
    }

    /**
     * @return mixed
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @return mixed
     */
    public function getWidgetVersion()
    {
        return $this->widget_version;
    }

    /**
     * @return mixed
     */
    public function getHtmlBefore()
    {
        return $this->html_before;
    }

    /**
     * @return mixed
     */
    public function getHtmlAfter()
    {
        return $this->html_after;
    }

}
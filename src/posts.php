<?php
/**
 * Created by PhpStorm.
 * User: dpaz
 * Date: 3/28/18
 * Time: 7:26 PM
 */

namespace dpazuic\curator_io\feeds;


class posts
{

    private $succes;
    private $pagination;
    private $networks;
    private $sources;
    private $postCount;
    private $posts;

    public function __construct($object = null)
    {

        if(empty($object)){

            throw new \Exception("Object is null ");

        }

        if(!is_object($object)){

            throw new \Exception("Object is not valid");

        }

        $this->mapProperties($object);
    }

    /**
     * @param $object
     */
    public function mapProperties($object)
    {

        foreach($object as $prop=>$val) {

            switch($prop){
                case "posts":
                    foreach($val as $post){
                        $this->posts[] = new \dpazuic\curator_io\feeds\post($post);
                    }
                    break;

                default;
                    $this->{$prop} = $val;
                    break;
            }

        }

    }

    /**
     * @return mixed
     */
    public function getPostCount()
    {

        return $this->postCount;

    }

    /**
     * @return mixed
     */
    public function getPosts()
    {

        return $this->posts;

    }

    /**
     * @return mixed
     */
    public function getSucces()
    {
        return $this->succes;
    }

    /**
     * @return mixed
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @return mixed
     */
    public function getNetworks()
    {
        return $this->networks;
    }

    /**
     * @return mixed
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * @param $postArray
     */
    public function appendPostArray($postArray)
    {

        $this->posts = array_merge($this->posts, $postArray);

    }

    /**
     * @param $post
     */
    public function appendPost($post)
    {

        $this->posts = array_push($this->posts, $post);

    }

}
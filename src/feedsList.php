<?php
/**
 * Created by PhpStorm.
 * User: dpaz
 * Date: 3/28/18
 * Time: 5:34 PM
 */

namespace dpazuic\curator_io\feeds;


class feedsList
{
    private $list;

    public function __construct($data = null)
    {

        if(!is_array($data)){

            throw new \Exception("Feeds JSON object array is null");

        }

        // Iterate over the $data array, setting each internal stdClass obj to a Feed Obj
        foreach($data as $feed){

            $this->list[] = new \dpazuic\curator_io\feeds\feed($feed);

        }

    }

    /**
     * @return mixed
     */
    public function getList()
    {
        return $this->list;
    }

}
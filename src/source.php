<?php
/**
 * Created by PhpStorm.
 * User: dpaz
 * Date: 3/28/18
 * Time: 5:37 PM
 */

namespace dpazuic\curator_io\sources;


class source
{

    // todo - make widget_options and colour_options objects
//[widget_options] => {"type":"Waterfall","maxHeight":0,"postsPerPage":12,"lang":"en","postClickAction":"open-popup","showShareIcons":true,"showReadMorePost":false,"showReadMorePopup":false,"waterfall":{"showLoadMore":true,"continuousScroll":false,"gridWidth":300,"animate":true,"animateSpeed":400},"grid":{"minWidth":200,"showLoadMore":false,"rows":3},"carousel":{"autoPlay":true,"infinite":true,"minWidth":200},"panel":{},"filter":{"showNetworks":false,"networksLabel":"SourceNetworks:","showSources":false,"sourcesLabel":"Sources:"}}
//[colour_options] => {"headerBg":"#ffffff","headerText":"#333333","bodyBg":"#ffffff","bodyText":"#333333","bodyLinks":"#000000","icon":"#000000","userName":"","date":"#000000","footerText":"#000000","footerBg":"#ffffff","footerIcons":"#333333","loadMoreText":"#2b2b2b","loadMoreBg":"#ffffff","loadMoreBorder":"rgba(0,0,0,0.1)"}

    private $id;
    private $feed_id;
    private $source_type;
    private $network_id;
    private $tag;
    private $name;
    private $interval;
    private $post_per_minute;
    private $post_per_hour;
    private $update_posts_processed_at;
    private $new_posts_processed_at;
    private $first_post_date;
    private $status;
    private $initialised;
    private $error_count;
    private $post_count;
    private $additional_data;
    private $feed;
    private $html_after;

    public function __construct($object = null)
    {

        if(empty($object)){

            throw new \Exception("Object is null ");

        }

        if(!is_object($object)){

            throw new \Exception("Object is not valid");

        }

        $this->mapProperties($object);
    }

    /**
     * @param $object
     */
    public function mapProperties($object)
    {

        foreach($object as $prop=>$val) {

            if($prop == "feed") {

                $this->{$prop} = new \dpazuic\curator_io\feeds\feed($val);

            } else {

                $this->{$prop} = $val;

            }

        }

    }

}
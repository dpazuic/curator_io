<?php
/**
 * Created by PhpStorm.
 * User: Daniel-Paz-Horta
 * Date: 3/28/18
 * Time: 2:13 PM
 */

namespace dpazuic;


class curator_io
{

    const CURATOR_IO_API = 'https://api.curator.io/v1';

    /**
     * @var null
     */
    private $apiKey;

    /**
     * curator_io constructor.
     * @param null $apiKey
     * @throws \Exception
     */
    public function __construct($apiKey = null)
    {

        if(empty($apiKey)){

            throw new \Exception("API Key was not provided");

        }

        $this->apiKey = $apiKey;

    }

    /**
     * @return mixed
     */
    public function getSources()
    {

        $json = $this->sendRequest(constant("\dpazuic\\endpoints::SOURCES"));

        // Return a feedsList Object
        $sourcesList = new \dpazuic\curator_io\sources\sourcesList(json_decode($json));

        return $sourcesList->getList();

    }

    /**
     * @return mixed
     */
    public function getFeeds()
    {

        $json = $this->sendRequest(constant("\dpazuic\\endpoints::FEEDS"));

        // Return a feedsList Object
        $feedsList = new \dpazuic\curator_io\feeds\feedsList(json_decode($json));

        return $feedsList->getList();

    }

    /**
     * @param null $feedID
     * @param int $limit
     * @param int $offset
     * @return curator_io\feeds\posts
     * @throws \Exception
     */
    public function getPosts($feedID = null, $limit = 100, $offset = 0)
    {

        if(empty($feedID)){

            throw new \Exception("FeedID was not specified");

        }

        $dynamicEndpoint = str_replace("{}", $feedID, constant("\dpazuic\\endpoints::POSTS"));

        $json = $this->sendRequest($dynamicEndpoint . "?limit=" . $limit . "&offset=" . $offset);

        // Return a Posts Object
        return new \dpazuic\curator_io\feeds\posts(json_decode($json));

    }

    /**
     * @param null $feedID
     * @return curator_io\feeds\posts
     */
    public function getAllPosts($feedID = null)
    {

        // Get initial posts
        $originalPosts = $this->getPosts($feedID);

        // Check to see postCount
        for($i=100;$i<=$originalPosts->getPostCount() - 1;$i += 100){

            $postPage = $this->getPosts($feedID, 100, $i);

            $posts = $postPage->getPosts();

            if(is_array($posts) AND count($posts) > 0) {

                $originalPosts->appendPostArray($posts);

                unset($postPage);

            }

        }

        return $originalPosts;

    }

    /**
     * @param null $endPoint
     * @param null $requestBody
     * @return string
     * @throws \Exception
     */
    private function sendRequest($endPoint = null, $requestBody = null)
    {

        if(empty($endPoint)){

            throw new \Exception("Endpoint was not specified");

        }

        // Run a cURL request
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::CURATOR_IO_API . $endPoint,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_VERBOSE => true,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic " . base64_encode($this->apiKey . ":"),
                "Content-Type: application/json",
                "Cache-Control: no-cache"
            )
        ));

        // JSON Response
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return json_encode("cURL Error #:" . $err);

        } else {

            return $response;

        }

    }

}
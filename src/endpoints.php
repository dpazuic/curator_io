<?php
/**
 * Created by PhpStorm.
 * User: Daniel-Paz-Horta
 * Date: 3/28/18
 * Time: 2:19 PM
 */

namespace dpazuic;


abstract class endpoints extends \dpazuic\utilities\BasicEnum {
    const FEEDS = '/feeds';
    const FEED = '/feed/';
    const SOURCES = '/sources';
    const SOURCE = '/sources/';
    const POSTS = '/feeds/{}/posts';
}
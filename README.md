# curator_io

Use with composer by including `include_once('vendor/autoload.php');` on your project page.

##Instantiate curator_io
Instantiate curator_io by creating a new instance of `\dpazuic\curator_io(YOUR_API_KEY);`. Note, replace YOUR_API_KEY with the API key curator_io provides you.

## Use methods
Use any of the included methods Be mindful that some of the methods require extra parameters to be passed


```php
include_once('vendor/autoload.php');

$curatorIO = new \dpazuic\curator_io("SOME_KEY");

// Get Sources
$curatorIO->getSources();

// Get Feeds
$curatorIO->getFeeds();

// Get All Posts
$curatorIO->getAllPosts("SOME_FEED_ID");

// Get first 100 Posts
$curatorIO->getPosts("SOME_FEED_ID");

// Get next 30 Posts (after the first 100 posts)
$curatorIO->getPosts("SOME_FEED_ID", 30, 100);

```